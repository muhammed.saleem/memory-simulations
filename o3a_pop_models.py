def f(x,dm):
    return np.exp(1.0*dm/x + 1.0*dm/(x-dm))


#def S(m, mmin, dm):
#    return np.piecewise(m, [m < mmin, mmin <= m < (mmin +dm), m >= (mmin + dm)], [0, 1./(f(m-mmin, dm) + 1), 1])

def S(m, mmin, dm):
    if m < mmin:
        return 0.0
    if mmin <= m < (mmin +dm):
        return 1./(f(m-mmin, dm) + 1)
    if m >= (mmin + dm):
        return 1.0


    
def Beta (m1, alpha, mmin, mmax):
    
    Norm = (1-alpha)/ (pow(mmax, 1-alpha) - pow(mmin, 1-alpha) )
    if mmin < m1 < mmax:
        return Norm * m1**(-alpha)
    else:
        return 0.0
    

def G(m1, mu, sigma):
    return ss.norm.pdf(m1,mu,sigma)


def m1prior (m1, Lp, alpha, mmin, dm, mmax, mu, sigma):
    mixture = (1-Lp) * Beta (m1, alpha, mmin, mmax) + Lp * G(m1, mu, sigma)
    smoothener = S(m1, mmin, dm)
    return mixture * smoothener
    

def qprior (q, Bq, m1, mmin, dm):
    return q**Bq * S(q*m1, mmin, dm)


# spin distribution

# we are using the spin distributions given in
# https://journals.aps.org/prd/abstract/10.1103/PhysRevD.96.023012
# for spin magnitude distribution 
# and  https://arxiv.org/abs/1805.06442 for spin orientation distribution

def p_beta (x,alpha=2.750,beta=6.0,B=0.00814420334108462):
    return x**(alpha-1) * (1-x)**(beta-1) / B

def alpha_beta_from_moments (mu,var):
    alpha = mu**2 * ((1.0-mu)/var - (1./mu))
    beta  = alpha * (1./mu -1 )
    return alpha, beta


def p_beta_from_moments (x,mu,var,B=1):
    alpha, beta = alpha_beta_from_moments (mu,var)
    return p_beta (x,alpha,beta,B)

def normB (alpha=2.750,beta=6.0):
    du = 0.01
    u  = np.arange(du,1.0,du)
    integrand =  p_beta (u,alpha,beta,B=1) * du
    return np.sum(integrand)



file = '../data/o1o2o3_mass_c_iid_mag_two_comp_iid_tilt_powerlaw_redshift_result.json'
pos = bilby.result.read_in_result(filename = file)
pos.posterior

Lp = np.median(np.array(pos.posterior['lam']))
alpha = np.median(np.array(pos.posterior['alpha']))
mmin = np.median(np.array(pos.posterior['mmin']))
dm = np.median(np.array(pos.posterior['delta_m']))
mmax = np.median(np.array(pos.posterior['mmax']))
mu = np.median(np.array(pos.posterior['mpp']))
sigma = np.median(np.array(pos.posterior['sigpp']))
Bq = np.median(np.array(pos.posterior['beta']))

x = np.linspace(1,100,200)
px = x*0.0
for i in range(len(x)):
    px[i] = m1prior (x[i], Lp, alpha, mmin, dm, mmax, mu, sigma)

m1prior_interp =  si.interp1d(x, px, kind='slinear')


xi = np.median(np.array(pos.posterior['xi_spin']))
sigt = np.median(np.array(pos.posterior['sigma_spin']))
sigChi = np.median(np.array(pos.posterior['sigma_chi']))
muChi = np.median(np.array(pos.posterior['mu_chi']))



def spin_mag (x):
    daa = 0.01
    aa = np.arange(0,1,daa)
    norm = np.sum(p_beta_from_moments (aa,muChi,sigChi**2,B=1)) * daa
    return p_beta_from_moments (x,muChi,sigChi**2,B=1)/norm





def prior_0_m1 (m1):
    ppp = post.priors['mass_1']
    return ppp.prob(m1)
    
def prior_1_m1 (m1, min=3, max=100):
    dm = 1
    m1grid = np.arange(min,max,dm)
    norm = np.sum(m1prior_interp(m1grid)) * dm   
    return m1prior_interp(m1)/norm

def make_uniform_qprior (qmin, qmax):
    q = np.linspace(0,1,5000)
    probq = np.zeros(q.shape)
    probq[(q>=qmin)&(q<=qmax)] = 1./(qmax-qmin)
    return si.interp1d(q,probq)

prior_0_mass_ratio = make_uniform_qprior (qmin=0.125, qmax=1)


def prior_1_mass_ratio(q, m1, qmin=0.125, qmax=1.0):
    delta_q = 0.01
    qgrid = np.arange(qmin,qmax,delta_q)
    norm = 0.0
    for i in range(len(qgrid)):
        norm += qprior (qgrid[i], Bq, m1, mmin, dm)
    norm *= delta_q
    return qprior (q, Bq, m1, mmin, dm)/norm

def prior_0_spinmag (a):
    ppp = post.priors['a_1']
    return ppp.prob(a)

def prior_1_spinmag (a):
    return spin_mag (a)


lumdist_0 =  bilby.gw.prior.UniformComovingVolume(name='luminosity_distance', minimum=10, maximum=10000, unit='Mpc')
lumdist_1 =  bilby.gw.prior.UniformComovingVolume(name='luminosity_distance', minimum=10, maximum=8000, unit='Mpc')
    
    

def recycle (psamples, params, prior_0, prior_1):
    '''
    TODO: Not yet tested - need some fixes
    psamples: should have only those columns for which prior_0 and 1 are different.
    params  : parameters of psamples that have different priors in prior_0 and prior_1
    prior_0 and prior_1 : Old  and new priors
            should be functions or KDEs if the psamples have only one column
            if multiple (Ni) columns in psamples, 
            then both prior_0 and prior_1 should be list of Ni functions or KDEs
    '''
    prior_0 = list(prior_0)
    prior_1 = list(prior_1)
    
    rw_factor = 1.0
    for param, pr0, pr1 in zip(params, prior_0, prior_1):
        theta = psamples[param]
        rw_factor *= (pr1 (theta) / pr0(theta))
        
    return np.sum(rw_factor)/len(psamples)

    
def recycle_mass_spin (psamples):
    '''
    the function does prior re-weighting
    specifically for mass1, ,mass_ratio, a_1, a_2 (spin magnitudes)
    Distance prior is same for injections and recovery
    '''
    m1qpos = np.array([psamples.mass_1, psamples.mass_ratio]).transpose()
    a1pos = np.array(psamples.a_1)
    a2pos = np.array(psamples.a_2)
    dLpos = np.array(psamples.luminosity_distance)

    zero = 1e-100
    rw_factor = 0.0
    n_i = len(psamples)

    log_prior_ratio =  1e-100
    log_prior_ratio += np.log(zero + m1q_kde(m1qpos.transpose()))
    log_prior_ratio += np.log(zero + prior_1_spinmag (a1pos))
    log_prior_ratio += np.log(zero + prior_1_spinmag (a2pos))
    log_prior_ratio += np.log(zero + lumdist_1.prob(dLpos))
    
    

    log_prior_ratio -= np.log(zero + prior_0_m1 (m1qpos[:,0]))
    log_prior_ratio -= np.log(zero + prior_0_mass_ratio (m1qpos[:,1]))
    log_prior_ratio -= np.log(zero + prior_0_spinmag (a1pos))
    log_prior_ratio -= np.log(zero + prior_0_spinmag (a2pos))
    log_prior_ratio -= np.log(zero + lumdist_0.prob(dLpos))

    log_rw_factor = np.log(np.sum(np.exp(log_prior_ratio))/n_i)
    
    return log_rw_factor